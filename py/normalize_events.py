#!/usr/bin/python
"""
Event models

* impression start: device_id,device_type,user_id,event_ts,event_type_id,ad_id,impression_id,creative_type_id
* impression end: device_id,device_type,user_id,event_ts,event_type_id,ad_id,impression_id,creative_type_id,duration,
    video_name,buffer_time,percent_watched
* click: device_id,device_type,user_id,event_ts,event_type_id,ad_id,impression_id,creative_type_id

Responsible for normalizing events

Example spinning up cluster:

    aws emr create-cluster --name "clickstream analytics" --ami-version 3.10 --applications Name=Hue Name=Hive \
        Name=Pig --use-default-roles --ec2-attributes KeyName=edjohn-prod --instance-type m3.xlarge \
        --instance-count 3 --log-uri s3://indore-clickstream-logging/clickstream-analytics

Example adding streaming step (NOTE: incorrect split on multiple lines):

    aws emr add-steps --cluster-id j-3N89KJOLQQERY --steps 'Type=STREAMING,Name="normalize_events",\
        ActionOnFailure=CONTINUE,Args=[--files,s3://indore-clickstream-code/py/normalize_events.py,\
        -mapper,normalize_events.py,-reducer,NONE,-input,s3://indore-clickstream-raw-events/y=2015/m=10/d=10/*,\
        -output,s3://indore-clickstream-normalized/y=2015/m=10/d=10,-jobconf,mapred.output.compression=true]'

"""
import sys
import simplejson as json
import dateutil.parser
from datetime import datetime

TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%S.%f+00:00"


def row_to_event_dict(line, reported_ts):
    """Converts row array into event json

    Args:
        row: event string emitted by device
        reported_ts: timestamp when event was reported

    Returns:
        event dict
    """
    row = line.split(',')

    event_type_id = int(row[4])
    creative_type_id = int(row[7])
    event = {
        "reported_ts": reported_ts.strftime(TIMESTAMP_FORMAT),
        "device_id": str(row[0]),
        "device_type": str(row[1]),
        "user_id": int(row[2]),
        "event_ts": dateutil.parser.parse(row[3]).strftime(TIMESTAMP_FORMAT),
        "event_type_id": event_type_id,
        "ad_id": int(row[5]),
        "impression_id": long(row[6]),
        "creative_type_id": creative_type_id}

    event["creative_type"] = "VIDEO" if creative_type_id == 2 else "HTML"

    # amend the event with descriptive type and event attributes
    if event_type_id == 0:
        event["event_type"] = "impression_start"
    elif event_type_id == 1:
        event["event_type"] = "impression_end"
        event["duration"] = long(row[8])
        if creative_type_id == 2:
            event["video_name"] = str(row[9])
            event["buffer_time"] = long(row[10])
            event["percent_watched"] = int(row[11])
    elif event_type_id == 2:
        event["event_type"] = "click"
    else:
        raise Exception("line does not correspond to known event types")

    return event


def normalize(reported_ts, f_input=sys.stdin, f_output=sys.stdout, f_error=sys.stderr):
    """Reads input stream and normalizes lines to event model in json format.

    The following criteria must be met. Otherwise the lines are thrown out as malformed:
        * Meet one of three event models (impression, click, interaction)
        * Timestamps are no more than 24 hours into the future and no more

    Args:
        reported_ts: the timestamp of when the events were reported
        f_input: some input stream
        f_output: some output stream
        f_error: some error stream
    """
    for line in f_input:
        try:
            # get event dictionary
            event = row_to_event_dict(line, reported_ts)

            # write to output stream as json
            f_output.write("{0}\n".format(json.dumps(event)))
        except Exception, e:
            f_error.write("Exception processing line {0}, ex: {1}\n".format(line, e))


def main(argv):
    try:
        if len(argv) > 1:
            reported_ts = dateutil.parser.parse(argv[1])
        else:
            reported_ts = datetime.now()
        normalize(reported_ts, sys.stdin, sys.stdout, sys.stderr)
    except Exception as e:
        sys.stderr.write("Exception while normalizing records in input, ex: {0}\n".format(e))
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv)
