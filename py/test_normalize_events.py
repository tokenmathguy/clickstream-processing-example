#!/usr/bin/python
import pytest
from datetime import datetime
from normalize_events import row_to_event_dict


@pytest.fixture
def impression_start():
    return "AB1234,MOB1,123,2000-01-02 03:04:05.678,0,9876,112233,1"


@pytest.fixture
def impression_end():
    return "AB1234,MOB1,123,2000-01-02 03:04:05.678,1,9876,112233,1,1445"


@pytest.fixture
def click():
    return "AB1234,MOB1,123,2000-01-02 03:04:05.678,2,9876,112233,1"


def test_row_to_event_dict_impression_start(impression_start):
    expected = {
        "reported_ts": "2000-01-02T03:00:00.000000+00:00",
        "device_id": "AB1234",
        "device_type": "MOB1",
        "user_id": 123,
        "event_ts": "2000-01-02T03:04:05.678000+00:00",
        "event_type_id": 0,
        "ad_id": 9876,
        "impression_id": 112233,
        "creative_type_id": 1,
        "event_type": "impression_start",
        "creative_type": "HTML"
    }
    actual = row_to_event_dict(impression_start, datetime.strptime("2000-01-02T03", "%Y-%m-%dT%H"))
    assert actual == expected


def test_row_to_event_dict_impression_end(impression_end):
    expected = {
        "reported_ts": "2000-01-02T03:00:00.000000+00:00",
        "device_id": "AB1234",
        "device_type": "MOB1",
        "user_id": 123,
        "event_ts": "2000-01-02T03:04:05.678000+00:00",
        "event_type_id": 1,
        "ad_id": 9876,
        "impression_id": 112233,
        "creative_type_id": 1,
        "duration": 1445,
        "event_type": "impression_end",
        "creative_type": "HTML"
    }
    actual = row_to_event_dict(impression_end, datetime.strptime("2000-01-02T03", "%Y-%m-%dT%H"))
    assert actual == expected


def test_row_to_event_dict_click(click):
    expected = {
        "reported_ts": "2000-01-02T03:00:00.000000+00:00",
        "device_id": "AB1234",
        "device_type": "MOB1",
        "user_id": 123,
        "event_ts": "2000-01-02T03:04:05.678000+00:00",
        "event_type_id": 2,
        "ad_id": 9876,
        "impression_id": 112233,
        "creative_type_id": 1,
        "event_type": "click",
        "creative_type": "HTML"
    }
    actual = row_to_event_dict(click, datetime.strptime("2000-01-02T03", "%Y-%m-%dT%H"))
    assert actual == expected
