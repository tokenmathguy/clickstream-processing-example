# indore-clickstream
artifacts for clickstream processing of data for indore project

examples include:

* generation of test events at volume
* python streaming to normalize events
* pig deduplication of records and splitting of feeds
* hive ingestion and aggregation
