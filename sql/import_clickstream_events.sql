create table if not exists ads (
    ad_id int,
    campaign_id int,
    ad_name varchar(32)
)
diststyle all
sortkey (ad_id, campaign_id);

copy ads
from 's3://indore-clickstream-metadata/ads/'
credentials 'aws_access_key_id=<access-key-id>;aws_secret_access_key=<secret-access-key>'
region 'ap-southeast-1'
csv;

create table if not exists ads_device_types (
    ad_id int,
    device_type_id int
)
diststyle all
sortkey (ad_id, device_type_id);

copy ads_device_types
from 's3://zed-indore-clickstream-metadata/ads_device_types/'
credentials 'aws_access_key_id=<access-key-id>;aws_secret_access_key=<secret-access-key>'
region 'ap-southeast-1'
csv;

create table if not exists campaigns (
    campaign_id int,
    campaign_name varchar(32),
    advertiser_name varchar(32),
    campaign_start_date datetime,
    campaign_end_date datetime
)
diststyle all
sortkey (campaign_id);

copy campaigns
from 's3://zed-indore-clickstream-metadata/campaigns/'
credentials 'aws_access_key_id=<access-key-id>;aws_secret_access_key=<secret-access-key>'
region 'ap-southeast-1'
csv;

create table if not exists creative_types (
    creative_type_id int,
    creative_type_name varchar(32)
)
diststyle all
sortkey (creative_type_id);

copy creative_types
from 's3://zed-indore-clickstream-metadata/creative_types/'
credentials 'aws_access_key_id=<access-key-id>;aws_secret_access_key=<secret-access-key>'
region 'ap-southeast-1'
csv;

create table if not exists device_types (
    device_type_id int,
    device_type_name varchar(32)
)
diststyle all
sortkey (device_type_id);

copy device_types
from 's3://zed-indore-clickstream-metadata/device_types/'
credentials 'aws_access_key_id=<access-key-id>;aws_secret_access_key=<secret-access-key>'
region 'ap-southeast-1'
csv;

create table if not exists users (
    user_id int,
    first_name varchar(24),
    last_name varchar(24),
    gender varchar(8),
    age int,
    income bigint,
    profession varchar(24),
    mobile varchar(24),
    desktop varchar(24),
    logpart int
)
diststyle all
sortkey (user_id);

copy users
from 's3://zed-indore-clickstream-metadata/users/'
credentials 'aws_access_key_id=<access-key-id>;aws_secret_access_key=<secret-access-key>'
region 'ap-southeast-1'
csv;

create table if not exists traffic_events (
    reported_ts varchar(32),
    event_ts varchar(32),
    user_id int,
    device_id varchar(32),
    device_type_id int,
    device_type_name varchar(32),
    ad_id int,
    ad_name varchar(32),
    campaign_id int,
    campaign_name varchar(32),
    advertiser_name varchar(32),
    campaign_start_date varchar(32),
    campaign_end_date varchar(32),
    event_type_id int,
    event_type_name varchar(32),
    creative_type_id int,
    creative_type_name varchar(32),
    impression_id bigint
)
diststyle even
sortkey (reported_ts, event_ts, ad_id);

copy traffic_events
from 's3://indore-clickstream-analytics/traffic_events/y=2015/m=10/d=16/'
credentials 'aws_access_key_id=AKIAJZR7REMI6N6ARZYQ;aws_secret_access_key=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'
region 'ap-southeast-1'
json 'auto';

create table if not exists view_events (
    reported_ts varchar(32),
    event_ts varchar(32),
    user_id int,
    device_id varchar(32),
    device_type_id int,
    device_type_name varchar(32),
    ad_id int,
    ad_name varchar(32),
    campaign_id int,
    campaign_name varchar(32),
    advertiser_name varchar(32),
    campaign_start_date varchar(32),
    campaign_end_date varchar(32),
    event_type_id int,
    event_type_name varchar(32),
    creative_type_id int,
    creative_type_name varchar(32),
    impression_id bigint,
    duration bigint,
    video_name varchar(32),
    buffer_time bigint,
    percent_watched int
)
diststyle even
sortkey (reported_ts, event_ts, ad_id);

copy view_events
from 's3://indore-clickstream-analytics/view_events/y=2015/m=10/d=16/'
credentials 'aws_access_key_id=AKIAJZR7REMI6N6ARZYQ;aws_secret_access_key=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'
region 'ap-southeast-1'
json 'auto';

create table if not exists user_devices (
    reported_ts varchar(32),
    user_id bigint,
    device_id varchar(32),
    device_type_id int,
    device_type_name varchar(32),
    last_seen_ts varchar(32),
    total_seen_count bigint
)
diststyle even
sortkey (reported_ts, user_id, device_id);

copy user_devices
from 's3://20151018-1-indore-clickstream-analytics/user_devices/y=2015/m=09/d=19/'
credentials 'aws_access_key_id=AKIAJZR7REMI6N6ARZYQ;aws_secret_access_key=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'
region 'ap-southeast-1'
json 'auto';
