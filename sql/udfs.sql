create or replace function f_age_category (
    age int
) 
returns varchar 
immutable as $$
if age is None:
    return None
if age < 13:
    return "12 and under"
if age < 19:
    return "13 to 18"
if age < 25:
    return "19 to 24"
if age < 35:
    return "25 to 34"
if age < 45:
    return "35 to 44"
if age < 55:
    return "45 to 54"
if age < 65:
    return "55 to 64"
return "65 and older"
$$ LANGUAGE plpythonu;
