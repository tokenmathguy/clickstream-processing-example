# edit secure_path to include /usr/local/bin
sudo visudo

# install packages
sudo yum update
sudo yum groupinstall "Development Tools"
sudo yum install mysql-devel cyrus-sasl-devel libffi-devel postgresql94-devel

# pip install airflow requirements
sudo easy_install pip
sudo pip install --upgrade pip
sudo pip install airflow
sudo pip install airflow[mysql]
sudo pip install airflow[postgres]
sudo pip install airflow[hive]
sudo pip install airflow[crypto]
