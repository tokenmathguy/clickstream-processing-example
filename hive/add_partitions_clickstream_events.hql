--pulls clickstream data into hive cluster and aggregates
set hive.optimize.s3.query=true;
set hive.exec.parallel=true;
set hive.hadoop.supports.splittable.combineinputformat=true;
set hive.merge.mapfiles=true;

add jar s3://zed-indore-clickstream-code/jar/json-serde-1.3.6-jar-with-dependencies.jar;

alter table clickstream_events
add partition (y='${hiveconf:REPORT_YYYY}',m='${hiveconf:REPORT_MM}',d='${hiveconf:REPORT_DD}');

alter table users_to_devices 
add partition (y='${hiveconf:REPORT_YYYY}',m='${hiveconf:REPORT_MM}',d='${hiveconf:REPORT_DD}');
