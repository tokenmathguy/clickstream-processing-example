-- loads dim tables
--
-- aws emr add-steps --cluster-id j-39AD2Q3Z0RTKJ --steps 'Type=HIVE,Name="load_dimensions",ActionOnFailure=CONTINUE,\
--  Args=[-f,s3://indore-clickstream-code/hive/load_dimensions.hql]'
set hive.optimize.s3.query=true;
set hive.exec.parallel=true;
set hive.hadoop.supports.splittable.combineinputformat=true;
set hive.merge.mapfiles=true;

drop table if exists ads;
create external table ads (
    ad_id bigint,
    campaign_id bigint,
    ad_name string
)
row format delimited
fields terminated by ','
location 's3://indore-clickstream-metadata/ads';

drop table if exists ads_device_types;
create external table ads_device_types (
    ad_id bigint,
    device_type_id int
)
row format delimited
fields terminated by ','
location 's3://indore-clickstream-metadata/ads_device_types';

drop table if exists campaigns;
create external table campaigns (
    campaign_id bigint,
    campaign_name string,
    advertiser_name string,
    campaign_start_date string,
    campaign_end_date string
)
row format delimited
fields terminated by ','
location 's3://indore-clickstream-metadata/campaigns';

drop table if exists creative_types;
create external table creative_types (
    creative_type_id int,
    creative_type_name string
)
row format delimited
fields terminated by ','
location 's3://indore-clickstream-metadata/creative_types';

drop table if exists device_types;
create external table device_types (
    device_type_id int,
    device_type_name string
)
row format delimited
fields terminated by ','
location 's3://indore-clickstream-metadata/device_types';
