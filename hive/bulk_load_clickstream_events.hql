-- bulk loads clickstream events and users to deivces
--                                                                                                                                                                                                                                                                                                                             
-- aws emr add-steps --cluster-id j-39AD2Q3Z0RTKJ --steps 'Type=HIVE,Name="bulk_load_clickstream_events",\
--  ActionOnFailure=CONTINUE,Args=[-f,s3://indore-clickstream-code/hive/bulk_load_clickstream_events.hql]' 
set hive.optimize.s3.query=true;
set hive.exec.parallel=true;
set hive.hadoop.supports.splittable.combineinputformat=true;
set hive.merge.mapfiles=true;

add jar s3://indore-clickstream-code/jar/json-serde-1.3.6-jar-with-dependencies.jar;

drop table if exists clickstream_events;
create external table clickstream_events (
    reported_ts string,
    device_id string,
    device_type string,
    user_id bigint,
    event_ts string,
    event_type_id int,
    ad_id bigint,
    impression_id bigint,
    creative_type_id int,
    duration bigint,
    video_name string,
    buffer_time bigint,
    percent_watched int,
    event_type string,
    creative_type string
)
partitioned by (y string, m string, d string)
row format serde 'org.openx.data.jsonserde.JsonSerDe'
location 's3://indore-clickstream-events/';

msck repair table clickstream_events;

drop table if exists users_to_devices;
create external table users_to_devices (
    user_id bigint,
    device_id string,
    device_type string,
    max_event_ts string
)
partitioned by (y string, m string, d string)
row format serde 'org.openx.data.jsonserde.JsonSerDe'
location 's3://indore-clickstream-users/';

msck repair table users_to_devices;
