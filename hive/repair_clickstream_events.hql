-- bulk loads clickstream events and users to deivces
--                                                                                                                                                                                                                                                                                                                             
-- aws emr add-steps --cluster-id j-39AD2Q3Z0RTKJ --steps 'Type=HIVE,Name="bulk_load_clickstream_events",\
--  ActionOnFailure=CONTINUE,Args=[-f,s3://indore-clickstream-code/hive/bulk_load_clickstream_events.hql]' 
set hive.optimize.s3.query=true;
set hive.exec.parallel=true;
set hive.hadoop.supports.splittable.combineinputformat=true;
set hive.merge.mapfiles=true;

add jar s3://indore-clickstream-code/jar/json-serde-1.3.6-jar-with-dependencies.jar;

msck repair table clickstream_events;

msck repair table users_to_devices;
