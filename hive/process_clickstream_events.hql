--pulls clickstream data into hive cluster and aggregates
set hive.optimize.s3.query=true;
set hive.exec.parallel=true;
set hive.hadoop.supports.splittable.combineinputformat=true;
set hive.merge.mapfiles=true;

add jar s3://indore-clickstream-code/jar/json-serde-1.3.6-jar-with-dependencies.jar;

drop table if exists traffic_events;
create external table traffic_events (
    reported_ts string,
    event_ts string,
    user_id bigint,
    device_id string,
    device_type_id int,
    device_type_name string,
    ad_id bigint,
    ad_name string,
    campaign_id bigint,
    campaign_name string,
    advertiser_name string,
    campaign_start_date string,
    campaign_end_date string,
    event_type_id int,
    event_type_name string,
    creative_type_id int,
    creative_type_name string,
    impression_id bigint
)
row format serde 'org.openx.data.jsonserde.JsonSerDe'
location 's3://indore-clickstream-analytics/traffic_events/y=${hiveconf:REPORT_YYYY}/m=${hiveconf:REPORT_MM}/d=${hiveconf:REPORT_DD}/';

insert overwrite table traffic_events
select
    concat(substr(e.reported_ts, 1, 10), ' ', substr(e.reported_ts, 12, 9)) reported_ts,
    concat(substr(e.event_ts, 1, 10), ' ', substr(e.event_ts, 12, 9)) event_ts,
    e.user_id,
    e.device_id,
    d.device_type_id,
    d.device_type_name,
    e.ad_id,
    a.ad_name,
    a.campaign_id,
    c.campaign_name,
    c.advertiser_name,
    c.campaign_start_date,
    c.campaign_end_date,
    e.event_type_id,
    case when e.event_type = 'impression_start' then 'impression' else e.event_type end event_type_name,
    e.creative_type_id,
    cr.creative_type_name,
    e.impression_id
from clickstream_events e
join device_types d
on e.device_type = d.device_type_name
join ads a
on e.ad_id = a.ad_id
join campaigns c
on a.campaign_id = c.campaign_id
join creative_types cr
on e.creative_type_id = cr.creative_type_id
where e.y = '${hiveconf:REPORT_YYYY}'
and e.m = '${hiveconf:REPORT_MM}'
and e.d = '${hiveconf:REPORT_DD}'
and e.event_type in ('impression_start', 'click');

drop table if exists view_events;
create external table view_events (
    reported_ts string,
    event_ts string,
    user_id bigint,
    device_id string,
    device_type_id int,
    device_type_name string,
    ad_id bigint,
    ad_name string,
    campaign_id bigint,
    campaign_name string,
    advertiser_name string,
    campaign_start_date string,
    campaign_end_date string,
    event_type_id int,
    event_type_name string,
    creative_type_id int,
    creative_type_name string,
    impression_id bigint,
    duration bigint,
    video_name string,
    buffer_time bigint,
    percent_watched int
)
row format serde 'org.openx.data.jsonserde.JsonSerDe'
location 's3://indore-clickstream-analytics/view_events/y=${hiveconf:REPORT_YYYY}/m=${hiveconf:REPORT_MM}/d=${hiveconf:REPORT_DD}/';

insert overwrite table view_events
select
    concat(substr(e.reported_ts, 1, 10), ' ', substr(e.reported_ts, 12, 9)) reported_ts,
    concat(substr(e.event_ts, 1, 10), ' ', substr(e.event_ts, 12, 9)) event_ts,
    e.user_id,
    e.device_id,
    d.device_type_id,
    d.device_type_name,
    e.ad_id,
    a.ad_name,
    a.campaign_id,
    c.campaign_name,
    c.advertiser_name,
    c.campaign_start_date,
    c.campaign_end_date,
    e.event_type_id,
    'view' event_type_name,
    e.creative_type_id,
    cr.creative_type_name,
    e.impression_id,
    e.duration,
    e.video_name,
    e.buffer_time,
    e.percent_watched
from clickstream_events e
join device_types d
on e.device_type = d.device_type_name
join ads a
on e.ad_id = a.ad_id
join campaigns c
on a.campaign_id = c.campaign_id
join creative_types cr
on e.creative_type_id = cr.creative_type_id
where e.y = '${hiveconf:REPORT_YYYY}'
and e.m = '${hiveconf:REPORT_MM}'
and e.d = '${hiveconf:REPORT_DD}'
and e.event_type = 'impression_end';

drop table if exists user_devices;
create external table user_devices (
    user_id bigint,
    device_id string,
    device_type_id int,
    device_type_name string,
    last_seen_ts string,
    total_seen_count bigint
)
row format serde 'org.openx.data.jsonserde.JsonSerDe'
location 's3://indore-clickstream-analytics/user_devices/y=${hiveconf:REPORT_YYYY}/m=${hiveconf:REPORT_MM}/d=${hiveconf:REPORT_DD}/';

insert overwrite table user_devices
select
    u.user_id,
    u.device_id,
    d.device_type_id,
    d.device_type_name,
    max(concat(substr(u.max_event_ts, 1, 10), ' ', substr(u.max_event_ts, 12, 9))) last_seen_ts,
    count(1) total_seen_count
from users_to_devices u
join device_types d
on u.device_type = d.device_type_name
group by
    u.user_id,
    u.device_id,
    d.device_type_id,
    d.device_type_name;
