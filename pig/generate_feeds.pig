/* this script generates feed data for import into hive cluster and further analytics 

sample execution:

    aws emr add-steps --cluster-id j-212535DAFQ7FU --steps 'Type=PIG,Name="generate_feeds",ActionOnFailure=CONTINUE, \
        Args=[-f,s3://indore-clickstream-code/pig/generate_feeds.pig,-p,CODE=s3://indore-clickstream-code,\
        -p,DEFERRED=s3://indore-clickstream-deferred/y=2015/m=10/d=10/,\
        -p,USERS_TO_DEVICES=s3://indore-clickstream-users/y=2015/m=10/d=10/,\
        -p,EVENTS=s3://indore-clickstream-events/y=2015/m=10/d=10/,\
        -p,NORMALIZED=s3://indore-clickstream-normalized/y=2015/m=10/d=10/]'

*/
REGISTER $CODE/jar/elephant-bird-pig-4.10.jar;
REGISTER $CODE/jar/elephant-bird-core-4.10.jar;
REGISTER $CODE/jar/elephant-bird-hadoop-compat-4.10.jar;
REGISTER $CODE/jar/json-simple-1.1.1.jar;
REGISTER $CODE/jar/piggybank-0.14.0.jar;
REGISTER $CODE/jar/joda-time-2.8.2.jar;

SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.GzipCodec;

DEFINE ISOToUnix org.apache.pig.piggybank.evaluation.datetime.convert.ISOToUnix();

rmf $DEFERRED
rmf $USERS_TO_DEVICES
rmf $EVENTS

normalized_jsons = LOAD '$NORMALIZED' USING com.twitter.elephantbird.pig.load.JsonLoader() AS (json:map[]);
normalized = FOREACH normalized_jsons GENERATE
    (chararray)json#'reported_ts' AS reported_ts:chararray,
    (chararray)json#'device_id' AS device_id:chararray,
    (chararray)json#'device_type' AS device_type:chararray,
    (long)json#'user_id' AS user_id:long,
    (chararray)json#'event_ts' AS event_ts:chararray,
    (int)json#'event_type_id' AS event_type_id:int,
    (long)json#'ad_id' AS ad_id:long,
    (long)json#'impression_id' AS impression_id:long,
    (int)json#'creative_type_id' AS creative_type_id:int,
    (long)json#'duration' AS duration:long,
    (chararray)json#'video_name' AS video_name:chararray,
    (long)json#'buffer_time' AS buffer_time:long,
    (int)json#'percent_watched' AS percent_watched:int,
    (chararray)json#'event_type' AS event_type:chararray,
    (chararray)json#'creative_type' AS creative_type:chararray;

/* store events from future or in distant past to deferred bucket */
SPLIT normalized INTO
    deferred IF 
        ISOToUnix(reported_ts) - ISOToUnix(event_ts) > 30L * 24 * 60 * 60 * 1000 OR
        ISOToUnix(reported_ts) - ISOToUnix(event_ts) < -1L * 24 * 60 * 60 * 1000,
    valid_events OTHERWISE;
STORE deferred INTO '$DEFERRED' USING JsonStorage();

/* collapse duplicate events */
events = DISTINCT valid_events;

/* store user to device mappings */
pre_users_to_devices = FOREACH events GENERATE
    reported_ts,
    device_id,
    device_type,
    user_id,
    event_ts;
grouped_users_to_devices = GROUP pre_users_to_devices BY (reported_ts, device_id, device_type, user_id);
users_to_devices = FOREACH grouped_users_to_devices {
    e = LIMIT $1 1;
    GENERATE
        FLATTEN(e.reported_ts) AS reported_ts:chararray, 
        FLATTEN(e.device_id) AS device_id:chararray, 
        FLATTEN(e.device_type) AS device_type:chararray, 
        FLATTEN(e.user_id) AS user_id:long,
        MAX(pre_users_to_devices.event_ts) AS max_event_ts:chararray;
};
STORE users_to_devices INTO '$USERS_TO_DEVICES' USING JsonStorage();

/* store events */
STORE events INTO '$EVENTS' USING JsonStorage();
