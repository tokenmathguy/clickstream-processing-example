create table view_events (
    reported_ts varchar(32),
    event_ts varchar(32),
    user_id int,
    device_id varchar(32),
    device_type_id int,
    device_type_name varchar(32),
    ad_id int,
    ad_name varchar(32),
    campaign_id int,
    campaign_name varchar(32),
    campaign_start_date varchar(32),
    campaign_end_date varchar(32),
    event_type_id int,
    event_type_name varchar(32),
    creative_type_id int,
    creative_type_name varchar(32),
    impression_id bigint,
    duration bigint,
    video_name varchar(32),
    buffer_time bigint,
    percent_watched int
);

create table if not exists ads (
    ad_id int,
    campaign_id int,
    ad_name varchar(32)
);

create table if not exists ads_device_types (
    ad_id int,
    device_type_id int
);

create table if not exists campaigns (
    campaign_id int,
    campaign_name varchar(32),
    advertiser_name varchar(32),
    campaign_start_date datetime,
    campaign_end_date datetime
);

create table if not exists creative_types (
    creative_type_id int,
    creative_type_name varchar(32)
);

create table if not exists device_types (
    device_type_id int,
    device_type_name varchar(32)
);
create table if not exists users (
    user_id int,
    first_name varchar(24),
    last_name varchar(24),
    gender varchar(8),
    age int,
    income bigint,
    profession varchar(24),
    mobile varchar(24),
    desktop varchar(24),
    logpart int,
    age_category varchar(24)
);

create table if not exists traffic_events (
    reported_ts varchar(32),
    event_ts varchar(32),
    user_id int,
    device_id varchar(32),
    device_type_id int,
    device_type_name varchar(32),
    ad_id int,
    ad_name varchar(32),
    campaign_id int,
    campaign_name varchar(32),
    advertiser_name varchar(32),
    campaign_start_date varchar(32),
    campaign_end_date varchar(32),
    event_type_id int,
    event_type_name varchar(32),
    creative_type_id int,
    creative_type_name varchar(32),
    impression_id bigint
);

create or replace function f_age_category (age int)
returns varchar 
immutable as $$
if age is None:
    return None
if age < 13:
    return "12 and under"
if age < 19:
    return "13 to 18"
if age < 25:
    return "19 to 24"
if age < 35:
    return "25 to 34"
if age < 45:
    return "35 to 44"
if age < 55:
    return "45 to 54"
if age < 65:
    return "55 to 64"
return "65 and older"
$$ LANGUAGE plpythonu;
