from __future__ import print_function
import os
from airflow.operators import PythonOperator
from airflow.operators import PostgresOperator
from airflow.operators import DummyOperator
from airflow.sensors import S3KeySensor
from airflow.models import DAG
from datetime import datetime
from functools import partial


def delete_keys(bucket, key_prefix, aws_access_key_id, aws_secret_access_key, **kwargs):
    return


def add_emr_step(cluster_id, step_args, **kwargs):
    return


def get_emr_chain(op_name, cluster_id, source_bucket_keys, target_bucket_prefixes, step_args, **kwargs):
    delete_op = PythonOperator(
        task_id="delete_{0}".format(op_name),
        python_callable=partial(
            delete_keys,
            bucket=bucket,
            key_prefix=key_prefix,
            **kwargs),
        dag=dag)

    execute_op = PythonOperator(
        task_id="execute_{0}".format(op_name),
        python_callable=partial(
            add_emr_step,
            step_args=step_args,
            **kwargs),
        dag=dag)
    execute_op.set_upstream(delete_op)

    waitfor_op = S3KeySensor(
        task_id="waitfor_{0}".format(op_name),
        dag=dag)
    waitfor_op.set_upstream(execute_op)

    return waitfor_op

default_args = {
    'owner': 'airflow',
    'start_date': datetime.strptime("2015-09-26", "%Y-%m-%d"),
    'aws_access_key_id': os.environ['AWS_ACCESS_KEY_ID'],
    'aws_secret_access_key': os.environ['AWS_SECRET_ACCESS_KEY'],
    'cluster_id': os.environ['EMR_CLUSTER_ID'],
}
schedule_prefix = schedule_date.strftime("y=%Y/m=%m/d=%d")

dag = DAG(dag_id='process_clickstream', default_args=default_args)

raw_bucket = "zed-indore-clickstream-raw-events"
waitfor_raw_events = S3KeySensor(
    task_id="waitfor_raw_events",
    dag=dag)

normalize_bucket = "zed-indore-clickstream-events"
op_normalize = get_emr_chain(
    op_name="normalize_events",
    bucket=normalize_bucket,
    key_prefix=schedule_prefix,
    step_args="'Type=STREAMING,Name=normalize_events,ActionOnFailure=CONTINUE,"
              "Args=[--files,s3://zed-indore-clickstream-code/py/normalize_events.py,"
              "-mapper,\"normalize_events.py 2015-10-10\",-reducer,NONE,"
              "-input,s3://{0}/{1}/*,-output,s3://{2}/{1}]'".format(
                  raw_bucket, normalize_bucket, schedule_prefix),
    **default_args)

op_generate = get_emr_chain(
    op_name="generate_feeds",
    bucket=generate_bucket,
    key_prefix=schedule_prefix,
    step_args="'Type=PIG,Name=generate_feeds,ActionOnFailure=CONTINUE,"
              "Args=[-f,s3://zed-indore-clickstream-code/pig/generate_feeds.pig,"
              "-p,CODE=s3://zed-indore-clickstream-code,-p,"
              "DEFERRED=s3://zed-indore-clickstream-deferred/{0}/,"
              "-p,USERS_TO_DEVICES=s3://zed-indore-clickstream-users/{0}/,"
              "-p,EVENTS=s3://zed-indore-clickstream-events/{0}/,"
              "-p,NORMALIZED=s3://zed-indore-clickstream-normalized/{0}/]'".format(schedule_prefix),
    **default_args)
op_generate.set_upstream(op_normalize)

op_process = get_emr_chain(
    op_name="process_clickstream",
    bucket=process_bucket,
    key_prefix=schedule_prefix,
    step_args="'Type=HIVE,Name=process_clickstream,ActionOnFailure=CONTINUE,"
              "Args=[-f,s3://zed-indore-clickstream-code/hive/load_dimensions.hql]'",
    **default_args)
op_process.set_upstream(op_generate)

load_redshift = PostgresOperator(
    task_id='load_redshift',
    dag=dag)
load_redshift.set_upstream(op_process)
